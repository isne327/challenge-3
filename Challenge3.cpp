#include<iostream>
#include<cstdlib>
#include<ctime>
	
using namespace std;
const int a=10,b=11,c=3;
void array(char [][b]);
void display(char [][b]);
void random(int*,int*);
void walk(int,int*,int*,char[][b],bool*);
void raider_walk(int , char[][b] , bool*);

int main()
{
	srand(time(0));
	int x=0,y=0,count=1,input,row=0,coll=0;
	char table[a][b] , raider[count][c];
	bool status = false; 
	bool clear = false;
	
	while(status==false) //Run Until GameOver(Status=true)
	{
			for(int q=0 ; q<10 ; q++) //Stage 1-10
			{
			row=0;
			coll=0;
			cout<<"##################################"<<endl;
			cout<<"Round: "<<count<<endl<<endl;
			array(table);
			table[0][0] = 'F';
			table[9][9] = 'E';
				for(int i=0 ; i<count ;i++) //random Raider at the start
				{
					for(int j=0 ; j<c ; j++)
						{
							while(true)
							{
								random(&x,&y);
								if(table[x][y]=='-')
								{
									table[x][y] = 'R';
									break;
								}
							}
						}
				}
			display(table);
				clear=false;
				while(clear == false) //Run until you win (clear=true)
				{
					walk(input,&row,&coll,table,&clear);
					raider_walk(count,table,&status);
					display(table);
					cout << endl;
				}
			count=count+1; //Use for adding raider and display stage
			}
		if(count==11) //in case player clear all stage
		{
			cout << "Congrats you clear all stage!!!!"<<endl;
		}
	}
	if(count!=11) //in case player lose the game
	{
	cout << "You Lose Raider catch you" <<endl;
	}

}


void array(char array[][b]) //Fill in the array
{		
	for(int i=0; i<a; i++)
	{
		for(int j=0 ; j<b ;j++)
		{
			array[i][j] = '-';
		}
	}
	
	for(int k=0 ; k<a ; k++)
	{
		array[k][10]=' ';
	}
}

void display(char array[][b]) //Display the table
{
	for(int i=0; i<a; i++)
	{	
		for(int j=0 ; j<b-1 ;j++)
		{
			cout <<"|" << array[i][j] <<"|"<<" ";
		}
		cout << endl<<endl;;	
	}
}

void random(int *a,int *b) //Random Raider Position at the start
{
	*a=rand() %9;
	*b=rand() %9;
}

void walk(int input,int *row,int *coll,char table[][b],bool *clear) //Use to walk Frodo and Checking if you escape(case:6,8,9)
{
	int x=0;
	x=0;
	while(x==0)
	{
		while(true) //Check Valid Input
					{
						cin >> input;
						if(input==1 ||input==2||input==3||input==4||input==6||input==7||input==8||input==9)
						{
							break;
						}
						cout << "Invalid input please enter again (1-9)" <<endl;
					}
			switch(input)
			{
				case 7:
					if(table[*row-1][*coll-1]=='-')
					{
						swap(table[*row][*coll],table[*row-1][*coll-1]);
						x=1;
						*row=*row-1;
						*coll=*coll-1;
						display(table);
						cout << endl;
						break;
					}
					cout << "Invalid Move please enter again (1-9)" <<endl;
					break;
					
				case 8:
					if(table[*row-1][*coll]=='-')
					{
						swap(table[*row][*coll],table[*row-1][*coll]);
						x=1;
						*row=*row-1;
						*coll=*coll;
						break;
					}
					cout << "Invalid Move please enter again (1-9)" <<endl;
					break;
				
				case 9:
					if(table[*row-1][*coll+1]=='-')
					{
						swap(table[*row][*coll],table[*row-1][*coll+1]);
						*row=*row-1;
						*coll=*coll+1;
						x=1;
						break;
					}
					cout << "Invalid Move please enter again (1-9)" <<endl;
					break;
				
				case 4:
					 if(table[*row][*coll-1]=='-')
					{
						swap(table[*row][*coll],table[*row][*coll-1]);
						*row=*row;
						*coll=*coll-1;
						x=1;
						break;
					}
					cout << "Invalid Move please enter again (1-9)" <<endl;
					break;
					
				case 6:
					if(table[*row][*coll+1]=='E')
					{
						*clear=true;
						break;
					}
					else if(table[*row][*coll+1]=='-')
					{
						swap(table[*row][*coll],table[*row][*coll+1]);
						*row=*row;
						*coll=*coll+1;
						x=1;
						break;
					}
					cout << "Invalid Move please enter again (1-9)" <<endl;
					break;
				
				case 1:
					if(table[*row+1][*coll-1]=='-')
					{
						swap(table[*row][*coll],table[*row+1][*coll-1]);
						*row=*row+1;
						*coll=*coll-1;
						x=1;
						break;
					}
					cout << "Invalid Move please enter again (1-9)" <<endl;
					break;
				
				case 2:
					if(table[*row+1][*coll]=='E')
					{
						*clear=true;
						break;
					}
					else if(table[*row+1][*coll]=='-')
					{
						swap(table[*row][*coll],table[*row+1][*coll]);
						*row=*row+1;
						*coll=*coll;
						x=1;
						break;
					}
					cout << "Invalid Move please enter again (1-9)" <<endl;
					break;
					
				case 3:
					if(table[*row+1][*coll+1]=='E')
					{
						*clear=true;
						break;
					}
					else if(table[*row+1][*coll+1]=='-')
					{
						swap(table[*row][*coll],table[*row+1][*coll+1]);
						*row=*row+1;
						*coll=*coll+1;
						x=1;
						break;
					}
					cout << "Invalid Move please enter again (1-9)" <<endl;
					break;
			}
			break;
	}
}

void raider_walk(int count , char table[][b] , bool *status) //Use to walk Raiders and Check for Gameover ('F' Array)
{
	int x,k=0;
	for(int i = 0 ; i<10 ; i++)
	{
		for(int j=0 ; j<10 ; j++)
		{
			
			if(table[i][j]=='R')
			{	
				table[i][j]='K'; //in case raider move to the right
				k=0;
			//	while(k==0)
			//	{
					while(true) //random raider move
					{
					x=rand()%9;
						if(x==1 || x==2 || x==3 || x==4 || x==6 || x==7 || x==8 || x==9)
						{
							break;
						}
					}
						switch(x) //case + check for gameover
					{
						case 1:
							if(table[i-1][j-1]=='F')
							{
								*status=true;
							}
							else if(table[i-1][j-1]=='-')
							{
								swap(table[i][j],table[i-1][j-1]);
								k=1;
								break;
							}
							break;
							
						case 2:
							if(table[i-1][j]=='F')
							{
								*status=true;
							}
							else if(table[i-1][j]=='-')
							{
								swap(table[i][j],table[i-1][j]);
								k=1;
								break;
							}
							break;
						
						case 3:
							if(table[i-1][j+1]=='F')
							{
								*status=true;
							}
							else if(table[i-1][j+1]=='-')
							{
								swap(table[i][j],table[i-1][j+1]);
								k=1;
								break;
							}
							break;
						
						case 4:
							if(table[i][j-1]=='F')
							{
								*status=true;
							}
							else if(table[i][j-1]=='-')
							{
								swap(table[i][j],table[i][j-1]);
								k=1;
								break;
							}
							break;
							
						case 6:
							if(table[i][j+1]=='F')
							{
								*status=true;
							}
							else if(table[i][j+1]=='-')
							{
								swap(table[i][j],table[i][j+1]);
								k=1;
								break;
							}
							break;
						
						case 7:
							if(table[i+1][j-1]=='F')
							{
								*status=true;
							}
							else if(table[i+1][j-1]=='-')
							{
								swap(table[i][j],table[i+1][j-1]);
								k=1;
								break;
							}
							break;
						
						case 8:
							if(table[i+1][j]=='F')
							{
								*status=true;
							}
							else if(table[i+1][j]=='-')
							{
								swap(table[i][j],table[i+1][j]);
								k=1;
								break;
							}
							break;
							
						case 9:
							if(table[i+1][j+1]=='F')
							{
								*status=true;
							}
							else if(table[i+1][j+1]=='-')
							{
								swap(table[i][j],table[i+1][j+1]);
								k=1;
								break;
							}
							break;
					}
			//	}
			}
		}
	}
	
	for(int q=0 ; q<10 ; q++) // Change 'K' back to 'R'
	{
		for(int w=0 ; w<10 ; w++)
		{
			if(table[q][w]=='K')
			{
				table[q][w]='R';
			}
		}
	}
}










